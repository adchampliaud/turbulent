<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Player;

interface PlayerRepositoryInterface
{
    public function getById(int $id): ?Player;

    public function save(Player $player): void;
}