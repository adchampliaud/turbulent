<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Player;
use Doctrine\ORM\EntityManagerInterface;

final class PlayerRepository implements PlayerRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function getById(int $id): ?Player
    {
        return $this->entityManager->find(Player::class, $id);
    }

    public function save(Player $player): void
    {
        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }
}