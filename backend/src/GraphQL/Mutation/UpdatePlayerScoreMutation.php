<?php

declare(strict_types=1);

namespace App\GraphQL\Mutation;

use App\Repository\PlayerRepositoryInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;

final class UpdatePlayerScoreMutation implements MutationInterface, AliasedInterface
{

    public function __construct(
        private readonly PlayerRepositoryInterface $playerRepository,
    ) {
    }

    public function resolve(array $args, int $idPlayer, int $score): array
    {
        $player = $this->playerRepository->getById($idPlayer);

        if ($player === null) {
            return [
                'state' => 'error',
                'message' => sprintf('Player not found with id %s', $idPlayer),
            ];
        }
        $player->setScore($score);

        return [
            'state' => 'success',
            'message' => sprintf(
                'Player %s is update with score %s',
                $player->getNickname(),
                $player->getScore(),
        )];
    }

    public static function getAliases(): array
    {
        return [
            'resolve' => 'updatePlayerScore',
        ];
    }
}