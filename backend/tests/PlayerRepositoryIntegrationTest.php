<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\Player;
use App\Repository\PlayerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class PlayerRepositoryIntegrationTest extends KernelTestCase
{
    private readonly PlayerRepositoryInterface $playerRepository;
    private readonly EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        $this->playerRepository = self::getContainer()->get(PlayerRepositoryInterface::class);
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    /** @test */
    public function it_should_save_entity(): void
    {
        $expectedNumberOfPlayers = $this->getNumberOfPlayers() + 1;
        $nickname = 'test_nick_name';
        $this->playerRepository->save(new Player($nickname));
        self::assertEquals($expectedNumberOfPlayers, $this->getNumberOfPlayers());
    }

    private function getNumberOfPlayers(): int
    {
        return $this->entityManager->createQueryBuilder()
            ->select('count(1)')
            ->from(Player::class, 'p')
            ->getQuery()
            ->getSingleScalarResult();
    }
}